package de.czyrux.geocountry;

import android.app.Application;

import de.czyrux.geocountry.core.ServiceProvider;
import de.czyrux.geocountry.core.image.ImageService;
import de.czyrux.geocountry.core.image.ImageServiceFactory;
import de.czyrux.geocountry.core.rest.CountryService;
import de.czyrux.geocountry.core.rest.CountryServiceFactory;

public class GeoCountryApp extends Application implements ServiceProvider {

    private ImageService imageService;
    private CountryService repository;

    @Override
    public void onCreate() {
        super.onCreate();

        // Create service
        repository = CountryServiceFactory.createCountryService();
        imageService = ImageServiceFactory.createService(this);
    }

    @Override
    public ImageService getImageService() {
        return imageService;
    }

    @Override
    public CountryService getCountryRepository() {
        return repository;
    }
}
