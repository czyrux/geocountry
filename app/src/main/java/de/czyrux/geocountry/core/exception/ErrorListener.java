package de.czyrux.geocountry.core.exception;

public interface ErrorListener {
    public void onErrorOnRequest();
    public void onNoNetworkAvailable();
    public void onUnAuthorizedRequest();
}
