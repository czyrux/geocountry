package de.czyrux.geocountry.core.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

/**
 * Loader which extends AsyncTaskLoaders and handles caveats as pointed out in
 * http://code.google.com/p/android/issues/detail?id=14944.
 * <p/>
 *
 * @param <T> data type
 * @author Antonio Gutierrez
 */
public class AsyncLoader<T> extends AsyncTaskLoader<LoaderResult<T>> {

    private LoaderResult<T> result;
    private final LoaderTask<T> task;
    private volatile boolean loading;

    public AsyncLoader(Context ctx, LoaderTask<T> task) {
        super(ctx);
        if (task == null) {
            throw new IllegalArgumentException("LoaderTask cannot be null");
        }
        this.task = task;
    }

    @Override
    protected void onStartLoading() {
        if (result != null) {
            // Deliver any previously loaded data immediately.
            deliverResult(result);
        }

        if (takeContentChanged() || result == null) {
            // When the observer detects a change, it should call onContentChanged()
            // on the Loader, which will cause the next call to takeContentChanged()
            // to return true. If this is ever the case (or if the current data is
            // null), we force a new load.
            forceLoad();
        }
    }

    @Override
    public LoaderResult<T> loadInBackground() {
        loading = true;
        LoaderResult<T> data;
        try {
            T content = task.getData();
            data = new LoaderResult<>(content);

        } catch (Exception e) {
            data = new LoaderResult<>(e);
        }

        return data;
    }

    @Override
    protected void onStopLoading() {
        // The Loader is in a stopped state, so we should attempt to cancel the
        // current load (if there is one).
        cancelLoad();
    }

    @Override
    public void deliverResult(LoaderResult<T> dataLoaded) {
        loading = false;
        if (isReset()) {
            // The Loader has been reset; ignore the result and invalidate the data.
            result = null;
            return;
        }

        this.result = dataLoaded;

        if (isStarted()) {
            // If the Loader is in a started state, deliver the results to the
            // client. The superclass method does this for us.
            super.deliverResult(result);
        }
    }

    @Override
    protected void onReset() {
        // Ensure the loader has been stopped.
        onStopLoading();

        // Release resources.
        result = null;

    }

    public boolean isLoading() {
        return loading;
    }

    public boolean hasMoreData() {
        return task.hasMoreData();
    }

    public void load() {
        // It will not forced load unless there is nothing already loading
        if (!isLoading()) {
            super.forceLoad();
        }
    }
}
