package de.czyrux.geocountry.core;

import de.czyrux.geocountry.core.image.ImageService;
import de.czyrux.geocountry.core.rest.CountryService;

public interface ServiceProvider {

    public ImageService getImageService();

    public CountryService getCountryRepository();
}
