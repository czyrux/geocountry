package de.czyrux.geocountry.core.rest;

import de.czyrux.geocountry.model.Country;

public interface CountryCache {

    public void put(String name, Country country);

    public Country get(String name);
}
