package de.czyrux.geocountry.core.exception;

public class NoNetworkException extends Throwable {
    public NoNetworkException(String detailMessage) {
        super(detailMessage);
    }
}
