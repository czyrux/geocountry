package de.czyrux.geocountry.core.rest;

import java.util.HashMap;
import java.util.Map;

import de.czyrux.geocountry.model.Country;

public class CountryCacheImpl implements CountryCache {

    private final Map<String, Country> map;

    CountryCacheImpl() {
        map = new HashMap<String, Country>(20);
    }

    @Override
    public void put(String code, Country country) {
        map.put(code, country);
    }

    @Override
    public Country get(String code) {
        return map.get(code);
    }
}
