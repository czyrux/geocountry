package de.czyrux.geocountry.core.image;

import android.content.Context;

import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.ImageLoader;

public class ImageServiceFactory {

    public static ImageService createService(Context context) {
        HttpStack httpStack = new OkHttpStack();
        ImageLoader.ImageCache cache = new LruBitmapCache(LruBitmapCache.getCacheSize(context));

        return new ImageService(context, httpStack, cache);
    }
}
