package de.czyrux.geocountry.core.exception;

/**
 * Class holding logic regarding how to manage error received
 */
public class ErrorHandler {

    public ErrorHandler() {
    }

    public void handle(Throwable error, ErrorListener listener) {

        if (error instanceof NoNetworkException) {
            listener.onNoNetworkAvailable();
        } else if (error instanceof UnAuthorizedException){
            listener.onUnAuthorizedRequest();
        } else {
            listener.onErrorOnRequest();
        }
    }
}
