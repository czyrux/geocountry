package de.czyrux.geocountry.core.rest;

import java.util.List;

import de.czyrux.geocountry.model.Country;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public interface RestCountryApiService {

    @GET("/all")
    public List<Country> getAllCountries();

    @GET("/alpha")
    public List<Country> getCountriesByCodes(@Query("codes") String codes);

    @GET("/region/{region}")
    public List<Country> getCountriesByRegion(@Path("region") String region);
}
