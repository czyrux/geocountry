package de.czyrux.geocountry.core.loader;

/**
 * Result obtained from an AsyncLoader task.
 * @param <T> data type
 * @author Antonio Gutierrez
 */
public class LoaderResult<T> {

    private final T data;
    private final Exception errorException;

    public LoaderResult(T data) {
        this.data = data;
        this.errorException = null;
    }

    public LoaderResult(Exception errorException) {
        this.errorException = errorException;
        this.data = null;
    }

    public boolean isOk() {
        return data != null;
    }

    public T getData() {
        return data;
    }

    public Exception getErrorException() {
        return errorException;
    }
}
