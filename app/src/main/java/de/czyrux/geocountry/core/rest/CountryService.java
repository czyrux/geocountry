package de.czyrux.geocountry.core.rest;

import java.util.List;

import de.czyrux.geocountry.model.Country;

public class CountryService {

    private final RestCountryApiService restService;
    private final CountryCache cache;

    protected CountryService(RestCountryApiService service, CountryCache cache) {

        this.restService = service;
        this.cache = cache;
    }

    public List<Country> getAllCountries() {

        List<Country> countryList = restService.getAllCountries();
        if (countryList != null) {

            for (Country country : countryList) {
                cache.put(country.getAlpha2Code(), country);
            }
        }


        return countryList;
    }

    public Country getCountryByCode(String countryCode) {

        Country country = cache.get(countryCode);

        if (country == null)
        {
            List<Country> countryList = getCountriesByCode(new String[] { countryCode});
            if (countryList != null && countryList.size() >= 1)
            {
                country = countryList.get(0);
            }
        }

        return country;
    }

    public List<Country> getCountriesByCode(String[] countryCodes) {

        StringBuilder codesQuery = new StringBuilder();
        for (String code : countryCodes) {
            if (codesQuery.length() != 0) {
                codesQuery.append(';');
            }

            codesQuery.append(code);
        }

        List<Country> countryList = restService.getCountriesByCodes(codesQuery.toString());
        if (countryList != null) {

            // if a country code is not correct, the api will return an empty null object in place
            countryList.remove(null);

            for (Country country : countryList) {
                if (country != null) {
                    cache.put(country.getAlpha2Code(), country);
                }

            }
        }

        return countryList;
    }

    public List<Country> getCountriesByRegion(String region) {

        List<Country> countryList = restService.getCountriesByRegion(region);
        if (countryList != null) {

            // if a country code is not correct, the api will return an empty null object in place
            countryList.remove(null);

            for (Country country : countryList) {
                if (country != null) {
                    cache.put(country.getAlpha2Code(), country);
                }

            }
        }

        return countryList;
    }


}
