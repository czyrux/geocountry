package de.czyrux.geocountry.core.loader;

/**
 * Carry out the task of loading the data for AsyncLoader.
 * It can perform IO or network task.
 * @param <T> data type
 * @author Antonio Gutierrez
 */
public interface LoaderTask<T> {

    public T getData();

    boolean hasMoreData();
}
