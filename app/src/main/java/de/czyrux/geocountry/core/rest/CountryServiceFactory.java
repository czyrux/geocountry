package de.czyrux.geocountry.core.rest;

import retrofit.RestAdapter;

public class CountryServiceFactory {

    private static final String API_URL = "http://restcountries.eu/rest/v1";

    public static CountryService createCountryService() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .setErrorHandler(new RetrofitErrorHandler())
                .build();

        RestCountryApiService restService = restAdapter.create(RestCountryApiService.class);

        CountryCache cache = new CountryCacheImpl();

        return new CountryService(restService, cache);
    }
}
