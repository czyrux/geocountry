package de.czyrux.geocountry.core.exception;

public class ConnectionTimeoutException extends Throwable {

    public ConnectionTimeoutException(String detailMessage) {
        super(detailMessage);
    }
}
