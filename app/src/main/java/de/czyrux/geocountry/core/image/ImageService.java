package de.czyrux.geocountry.core.image;


import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class ImageService {

    private final Context context;
    private final HttpStack httpStack;
    private final ImageLoader.ImageCache cache;

    protected ImageService(Context context, HttpStack httpStack , ImageLoader.ImageCache cache) {
        this.context = context.getApplicationContext();
        this.httpStack = httpStack;
        this.cache = cache;
    }

    private RequestQueue newRequestQueue() {
        return Volley.newRequestQueue(context, httpStack);
    }

    public ImageLoader newImageLoader() {
        return new ImageLoader(newRequestQueue(), cache);
    }



}
