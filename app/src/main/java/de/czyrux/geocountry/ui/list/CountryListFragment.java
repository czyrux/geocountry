package de.czyrux.geocountry.ui.list;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;

import java.util.Arrays;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnItemClick;
import de.czyrux.geocountry.R;
import de.czyrux.geocountry.core.ServiceProvider;
import de.czyrux.geocountry.core.exception.ErrorHandler;
import de.czyrux.geocountry.core.rest.CountryService;
import de.czyrux.geocountry.model.Country;
import de.czyrux.geocountry.core.loader.AsyncLoader;
import de.czyrux.geocountry.core.loader.LoaderTask;
import de.czyrux.geocountry.core.loader.LoaderResult;
import de.czyrux.geocountry.ui.BaseFragment;

public class CountryListFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<LoaderResult<List<Country>>>, AbsListView.OnScrollListener {

    private static final String ARG_TITLE = "title";
    private static final String ARG_CODES = "codes";
    private static final String ARG_REGION = "region";
    private static final int LOADER_ID = 0;

    private CountryListListener mListener;
    private List<Country> countries;

    @InjectView(R.id.country_list_progressbar)
    ProgressBar mProgressBar;
    @InjectView(R.id.country_list)
    ListView mListView;

    public static CountryListFragment newInstanceByCodes(String screenTitle, String[] countryCodes) {

        Bundle args = new Bundle();
        args.putString(ARG_TITLE, screenTitle);
        args.putStringArray(ARG_CODES, countryCodes);

        CountryListFragment fragment = new CountryListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static CountryListFragment newInstanceByRegion(String region) {

        Bundle args = new Bundle();
        args.putString(ARG_REGION, region);

        CountryListFragment fragment = new CountryListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static CountryListFragment newInstanceByAll(String screenTitle) {
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, screenTitle);

        CountryListFragment fragment = new CountryListFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (CountryListListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement CountryListFragment.Listener");
        }
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    protected int getViewLayoutResourceId() {
        return R.layout.fragment_country_list;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListView.setOnScrollListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().initLoader(LOADER_ID, getArguments(), this);
    }

    private void populate() {

        if (countries != null) {
            ImageLoader loader = ((ServiceProvider) getActivity().getApplication()).getImageService().newImageLoader();

            CountryAdapter adapter = (CountryAdapter) mListView.getAdapter();
            if (adapter == null) {
                adapter = new CountryAdapter(loader);
                mListView.setAdapter(adapter);
            }
            adapter.setCountries(countries);

            mProgressBar.setVisibility(View.GONE);

            Bundle args = getArguments();
            if (args.containsKey(ARG_TITLE)) {
                mActionBarView.setActionBarTitle(args.getString(ARG_TITLE));
                mActionBarView.setActionBarSubTitle(args.containsKey(ARG_CODES) ? Arrays.toString(args.getStringArray(ARG_CODES))
                                : null);
            } else {
                mActionBarView.setActionBarTitle(args.getString(ARG_REGION));
                mActionBarView.setActionBarSubTitle(null);
            }
        } else {
            mProgressBar.setVisibility(View.VISIBLE);
        }

    }

    private void forceLoad() {
        Loader loader = getLoaderManager().getLoader(LOADER_ID);
        if (loader != null) {
            mProgressBar.setVisibility(View.VISIBLE);
            loader.forceLoad();
        }
    }

    private boolean isLoading() {
        AsyncLoader loader = (AsyncLoader) getLoaderManager().getLoader(LOADER_ID);
        if (loader != null) {
            return loader.isLoading();
        }

        return false;
    }

    private boolean hasMoreData() {
        AsyncLoader loader = (AsyncLoader) getLoaderManager().getLoader(LOADER_ID);
        if (loader != null) {
            return loader.hasMoreData();
        }

        return false;
    }

    @OnItemClick(R.id.country_list)
    public void onItemClick(int position) {
        Country country = (Country) mListView.getItemAtPosition(position);
        if (mListener != null) {
            mListener.onCountrySelected(country);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) { }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (firstVisibleItem >= (totalItemCount - visibleItemCount)
                && !isLoading()
                && hasMoreData()) {
            Toast.makeText(view.getContext(), "Requesting other page", Toast.LENGTH_SHORT).show();
            forceLoad();
        }
    }

    @Override
    public Loader<LoaderResult<List<Country>>> onCreateLoader(int id, Bundle args) {

        CountryService service = ((ServiceProvider) getActivity().getApplication()).getCountryRepository();
        LoaderTask<List<Country>> task;

        if (args != null && args.containsKey(CountryListFragment.ARG_CODES)) {
            task = new GetCountryListByCodesTask(service, args.getStringArray(CountryListFragment.ARG_CODES));
        } else if (args != null && args.containsKey(CountryListFragment.ARG_REGION)) {
            task = new GetCountryListByRegionTask(service, args.getString(CountryListFragment.ARG_REGION));
        } else {
            task = new GetAllCountryListTask(service);
        }

        return new AsyncLoader<>(getActivity(), task);
    }

    @Override
    public void onLoadFinished(Loader<LoaderResult<List<Country>>> loader, LoaderResult<List<Country>> data) {
        if (data.isOk()) {
            this.countries = data.getData();
            populate();
        } else {
            new ErrorHandler().handle(data.getErrorException().getCause(), mErrorListener);
        }
    }

    @Override
    public void onLoaderReset(Loader<LoaderResult<List<Country>>> loader) {
        this.countries = null;
    }
}
