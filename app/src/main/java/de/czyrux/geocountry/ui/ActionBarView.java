package de.czyrux.geocountry.ui;

public interface ActionBarView {
    void setActionBarTitle(String title);
    void setActionBarSubTitle(String subTitle);
}
