package de.czyrux.geocountry.ui.list;

import java.util.List;

import de.czyrux.geocountry.core.rest.CountryService;
import de.czyrux.geocountry.model.Country;
import de.czyrux.geocountry.core.loader.LoaderTask;

public class GetCountryListByCodesTask implements LoaderTask<List<Country>> {

    private final CountryService service;
    private final String[] countryCodes;

    public GetCountryListByCodesTask(CountryService service, String[] countryCodes) {
        this.countryCodes = countryCodes;
        this.service = service;
    }

    @Override
    public List<Country> getData() {
        return service.getCountriesByCode(countryCodes);
    }

    @Override
    public boolean hasMoreData() {
        return false;
    }
}
