package de.czyrux.geocountry.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import de.czyrux.geocountry.core.exception.ErrorListener;

public abstract class BaseFragment extends Fragment {

    protected ActionBarView mActionBarView;
    protected ErrorListener mErrorListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mErrorListener = (ErrorListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ErrorListener");
        }

        try {
            mActionBarView = (ActionBarView) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ActionBarView");
        }
    }

    @Override
    public void onDetach() {
        mErrorListener = null;
        mActionBarView = null;
        super.onDetach();
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getViewLayoutResourceId(), container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public final void onDestroyView() {
        ButterKnife.reset(this);
        super.onDestroyView();
    }

    protected abstract int getViewLayoutResourceId();
}
