package de.czyrux.geocountry.ui.detail;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.czyrux.geocountry.R;
import de.czyrux.geocountry.core.ServiceProvider;
import de.czyrux.geocountry.core.exception.ErrorHandler;
import de.czyrux.geocountry.core.helper.CountryImageBuilder;
import de.czyrux.geocountry.core.loader.AsyncLoader;
import de.czyrux.geocountry.core.loader.LoaderResult;
import de.czyrux.geocountry.core.rest.CountryService;
import de.czyrux.geocountry.model.Country;
import de.czyrux.geocountry.ui.BaseFragment;

public class CountryDetailFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<LoaderResult<Country>> {


    private static final String ARG_CODE = "code";
    private static final int LOADER_ID = 0;

    private final static NumberFormat NUMBER_FORMAT = NumberFormat.getInstance(Locale.getDefault());

    private CountryDetailListener mListener;
    private Country country;
    private ImageLoader mImageLoader;

    @InjectView(R.id.country_detail_content)
    View mContentView;
    @InjectView(R.id.country_detail_progressbar)
    ProgressBar mProgressBar;

    // Main block
    @InjectView(R.id.country_detail_flag)
    NetworkImageView mFlagImageView;
    @InjectView(R.id.country_detail_nativename)
    TextView mNativeNameTextView;

    @InjectView(R.id.country_detail_name)
    TextView mNameTextView;
    @InjectView(R.id.country_detail_capital)
    TextView mCapitalTextView;

    @InjectView(R.id.country_detail_demonym)
    TextView mDemonymTextView;

    @InjectView(R.id.country_detail_population)
    TextView mPopulationTextView;

    @InjectView(R.id.country_detail_area)
    TextView mAreaTextView;

    @InjectView(R.id.country_detail_gini)
    TextView mGiniTextView;

    // Location block
    @InjectView(R.id.country_detail_section_location)
    TextView mLocationTextView;
    @InjectView(R.id.country_detail_timezones)
    TextView mTimezonesTextView;
    @InjectView(R.id.country_detail_latitude)
    TextView mLatitudeTextView;
    @InjectView(R.id.country_detail_longitude)
    TextView mLongitudeTextView;

    @InjectView(R.id.country_detail_neighbour_countries)
    TextView mNeighboursTextView;
    @InjectView(R.id.country_detail_explore_neighbours)
    View mExploreNeighboursTextView;

    // Translations block
    @InjectView(R.id.country_detail_translations)
    LinearLayout mTranslationsLayout;

    // Misc block
    @InjectView(R.id.country_detail_callingcodes)
    TextView mCallingCodesTextView;
    @InjectView(R.id.country_detail_topleveldomain)
    TextView mTopLevelDomainTextView;
    @InjectView(R.id.country_detail_currency)
    TextView mCurrencyTextView;
    @InjectView(R.id.country_detail_languages)
    TextView mLanguagesTextView;
    @InjectView(R.id.country_detail_alphacode)
    TextView mAlphaCodeTextView;

    public static CountryDetailFragment newInstance(String countryCode) {
        CountryDetailFragment fragment = new CountryDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CODE, countryCode);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (CountryDetailListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement CountryDetailListener");
        }
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mImageLoader = ((ServiceProvider) getActivity().getApplication()).getImageService().newImageLoader();
    }

    @Override
    protected int getViewLayoutResourceId() {
        return R.layout.fragment_country_detail;
    }


    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().initLoader(LOADER_ID, getArguments(), this);
    }

    private void populate() {
        if (country != null) {
            mProgressBar.setVisibility(View.GONE);
            mContentView.setVisibility(View.VISIBLE);

            // Main block
            mFlagImageView.setImageUrl(CountryImageBuilder.obtainImageUrl(country), mImageLoader);

            mNativeNameTextView.setText(country.getNativeName());

            StringBuilder altNameBuilder = new StringBuilder();
            altNameBuilder.append(country.getName());

            for (String name : country.getAlternativeSpellings()) {
                altNameBuilder.append(", ").append(name);
            }

            mNameTextView.setText(altNameBuilder.toString());

            mCapitalTextView.setText(country.getCapital());
            mDemonymTextView.setText(country.getDemonym());
            mPopulationTextView.setText(NUMBER_FORMAT.format(country.getPopulation()));
            mAreaTextView.setText(NUMBER_FORMAT.format(country.getArea()));
            mGiniTextView.setText(NUMBER_FORMAT.format(country.getGini()));

            // Location block
            mLocationTextView.setText(country.getRegion() + ", " + country.getSubregion());
            mTimezonesTextView.setText(Arrays.toString(country.getTimezones()));


            if (country.getLatlong() != null && country.getLatlong().length == 2) {
                mLatitudeTextView.setText(String.valueOf(country.getLatlong()[0]));
                mLongitudeTextView.setText(String.valueOf(country.getLatlong()[1]));
            }

            if (country.getBorders() != null && country.getBorders().length > 0) {

                StringBuilder neighboursCountries = new StringBuilder();
                for (String countryCode : country.getBorders()) {
                    if (neighboursCountries.length() != 0) {
                        neighboursCountries.append(", ");
                    }

                    neighboursCountries.append(countryCode);
                }

                mNeighboursTextView.setText(neighboursCountries.toString());
            } else {
                mNeighboursTextView.setText(R.string.detail_no_neighbours);
                mExploreNeighboursTextView.setVisibility(View.GONE);
            }

            // Translations block
            mTranslationsLayout.removeAllViews();

            if (country.getTranslations() != null) {
                for (Map.Entry<String, String> translationEntry : country.getTranslations().entrySet()) {
                    View itemView = LayoutInflater.from(mContentView.getContext()).inflate(R.layout.fragment_country_detail_translation_item, mTranslationsLayout, false);
                    ((TextView) ButterKnife.findById(itemView, R.id.country_detail_translation_item_language)).setText(translationEntry.getKey());
                    ((TextView) ButterKnife.findById(itemView, R.id.country_detail_translation_item_translation)).setText(translationEntry.getValue());

                    mTranslationsLayout.addView(itemView);
                }
            }


            // Misc block
            mCallingCodesTextView.setText(Arrays.toString(country.getCallingCodes()));
            mTopLevelDomainTextView.setText(Arrays.toString(country.getTopLevelDomain()));
            mCurrencyTextView.setText(Arrays.toString(country.getCurrencies()));
            mLanguagesTextView.setText(Arrays.toString(country.getLanguages()));
            mAlphaCodeTextView.setText(country.getAlpha2Code() + ", " + country.getAlpha3Code());

            mActionBarView.setActionBarTitle(country.getName());
            mActionBarView.setActionBarSubTitle(null);

        } else {
            mContentView.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.country_detail_explore_region)
    public void onExploreRegion() {
        if (mListener != null) {
            mListener.onExploreCountryRegion(country);
        }
    }

    @OnClick(R.id.country_detail_explore_neighbours)
    public void onExploreNeighbours() {
        if (mListener != null) {
            mListener.onExploreCountryNeighbours(country);
        }
    }

    @Override
    public Loader<LoaderResult<Country>> onCreateLoader(int id, Bundle args) {

        CountryService service = ((ServiceProvider) getActivity().getApplication()).getCountryRepository();

        return new AsyncLoader<>(getActivity(), new GetCountryByCodeTask(service, args.getString(ARG_CODE)));
    }

    @Override
    public void onLoadFinished(Loader<LoaderResult<Country>> loader, LoaderResult<Country> data) {
        if (data.isOk()) {
            this.country = data.getData();
            populate();
        } else {
            new ErrorHandler().handle(data.getErrorException().getCause(), mErrorListener);
        }
    }

    @Override
    public void onLoaderReset(Loader<LoaderResult<Country>> loader) {
        this.country = null;
    }
}
