package de.czyrux.geocountry.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import de.czyrux.geocountry.R;
import de.czyrux.geocountry.core.exception.ErrorListener;
import de.czyrux.geocountry.model.Country;
import de.czyrux.geocountry.ui.detail.CountryDetailFragment;
import de.czyrux.geocountry.ui.detail.CountryDetailListener;
import de.czyrux.geocountry.ui.list.CountryListFragment;
import de.czyrux.geocountry.ui.list.CountryListListener;


public class GeoCountryActivity extends ActionBarActivity implements CountryListListener,
        CountryDetailListener, ErrorListener, ActionBarView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_country);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, CountryListFragment.newInstanceByAll(getString(R.string.title_allcountries)))
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_geo_country, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void replaceFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .setCustomAnimations(R.animator.push_left_in, R.animator.push_left_out, R.animator.push_right_in, R.animator.push_right_out)
                .replace(R.id.container, fragment)
                .commit();
    }

    @Override
    public void onCountrySelected(Country country) {
        replaceFragment(CountryDetailFragment.newInstance(country.getAlpha2Code()));
    }

    @Override
    public void onExploreCountryRegion(Country country) {
        replaceFragment(CountryListFragment.newInstanceByRegion(country.getRegion()));
    }

    @Override
    public void onExploreCountryNeighbours(Country country) {
        String screenTitle = getString(R.string.detail_neighbours_title).replace("{name}", country.getName());
        replaceFragment(CountryListFragment.newInstanceByCodes(screenTitle, country.getBorders()));
    }

    @Override
    public void onErrorOnRequest() {
        Toast.makeText(this, R.string.error_general, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNoNetworkAvailable() {
        Toast.makeText(this, R.string.error_network, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUnAuthorizedRequest() {
        // None
    }

    @Override
    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void setActionBarSubTitle(String subtitle) {
        getSupportActionBar().setSubtitle(subtitle);
    }
}
