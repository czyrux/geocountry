package de.czyrux.geocountry.ui.list;

import java.util.List;

import de.czyrux.geocountry.core.loader.LoaderTask;
import de.czyrux.geocountry.core.rest.CountryService;
import de.czyrux.geocountry.model.Country;

public class GetCountryListByRegionTask implements LoaderTask<List<Country>> {

    private final CountryService service;
    private final String region;

    public GetCountryListByRegionTask(CountryService service, String region) {
        this.region = region;
        this.service = service;
    }

    @Override
    public List<Country> getData() {
        return service.getCountriesByRegion(region);
    }

    @Override
    public boolean hasMoreData() {
        return false;
    }
}
