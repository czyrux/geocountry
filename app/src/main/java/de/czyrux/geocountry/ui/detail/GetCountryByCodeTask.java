package de.czyrux.geocountry.ui.detail;

import de.czyrux.geocountry.core.loader.LoaderTask;
import de.czyrux.geocountry.core.rest.CountryService;
import de.czyrux.geocountry.model.Country;

public class GetCountryByCodeTask implements LoaderTask<Country> {

    private final CountryService service;
    private final String countryCode;

    public GetCountryByCodeTask(CountryService service, String countryCode) {
        this.service = service;
        this.countryCode = countryCode;
    }

    @Override
    public Country getData() {
        return service.getCountryByCode(countryCode);
    }

    @Override
    public boolean hasMoreData() {
        return false;
    }
}
