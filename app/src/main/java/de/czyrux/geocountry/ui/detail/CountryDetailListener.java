package de.czyrux.geocountry.ui.detail;

import de.czyrux.geocountry.model.Country;

public interface CountryDetailListener {

    void onExploreCountryRegion(Country country);

    void onExploreCountryNeighbours(Country country);
}
