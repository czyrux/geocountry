package de.czyrux.geocountry.ui.list;

import de.czyrux.geocountry.model.Country;

public interface CountryListListener {
    void onCountrySelected(Country country);
}
