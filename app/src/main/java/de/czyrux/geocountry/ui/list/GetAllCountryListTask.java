package de.czyrux.geocountry.ui.list;

import java.util.ArrayList;
import java.util.List;

import de.czyrux.geocountry.core.loader.LoaderTask;
import de.czyrux.geocountry.core.rest.CountryService;
import de.czyrux.geocountry.model.Country;

public class GetAllCountryListTask implements LoaderTask<List<Country>> {

    private final static int ITEMS_PER_PAGE = 20;
    private final CountryService service;
    private int maxAvailableItems;
    private int currentPosition;

    private final List<Country> cacheCountries;
    private final List<Country> countries;

    public GetAllCountryListTask(CountryService service) {
        this.service = service;
        maxAvailableItems = 0;
        currentPosition = 0;
        cacheCountries = new ArrayList<>(ITEMS_PER_PAGE * 2);
        countries = new ArrayList<>(ITEMS_PER_PAGE * 2);
    }

    @Override
    public List<Country> getData() {

        // Fake server pagination
        if (cacheCountries.isEmpty()) {
            cacheCountries.addAll(service.getAllCountries());
            maxAvailableItems = cacheCountries.size();
        } else {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        int currentAvailableItems = (maxAvailableItems - currentPosition);
        int pageSize = Math.min(currentAvailableItems, ITEMS_PER_PAGE);

        List<Country> chunk = cacheCountries.subList(currentPosition, currentPosition + pageSize);
        currentPosition += pageSize;
        countries.addAll(chunk);

        return countries;
    }

    @Override
    public boolean hasMoreData() {
        return currentPosition < maxAvailableItems || currentPosition == 0;
    }
}
