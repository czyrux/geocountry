package de.czyrux.geocountry.ui.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.czyrux.geocountry.R;
import de.czyrux.geocountry.core.helper.CountryImageBuilder;
import de.czyrux.geocountry.model.Country;


public class CountryAdapter extends BaseAdapter {

    private final ImageLoader imageLoader;
    private final List<Country> countries;

    public CountryAdapter(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
        this.countries = new ArrayList<>(20);
    }

    public void setCountries(List<Country> countries) {
        this.countries.addAll(countries);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return countries.size();
    }

    @Override
    public Object getItem(int position) {
        return countries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Country country = (Country) getItem(position);

        CountryViewHolder viewHolder = null;
        if (convertView == null) {

            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_country_list_item, parent, false);
            viewHolder = new CountryViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (CountryViewHolder) convertView.getTag();
        }

        viewHolder.image.setImageUrl(CountryImageBuilder.obtainImageUrl(country), imageLoader);
        viewHolder.name.setText(country.getName());
        viewHolder.population.setText(String.format(Locale.GERMAN, "%,d", country.getPopulation()));
        viewHolder.region.setText(country.getRegion() + ", " + country.getSubregion());

        return convertView;
    }


    static class CountryViewHolder {
        @InjectView(R.id.country_item_image)
        NetworkImageView image;
        @InjectView(R.id.country_item_name)
        TextView name;
        @InjectView(R.id.country_item_population)
        TextView population;
        @InjectView(R.id.country_item_region)
        TextView region;

        CountryViewHolder(View parent) {
            ButterKnife.inject(this, parent);
        }
    }
}
